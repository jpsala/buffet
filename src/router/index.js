import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'
import VueCompositionApi from '@vue/composition-api'
Vue.use(VueRouter)
Vue.use(VueCompositionApi)
/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function ({ store }) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })
  const tryToLogWithToken = async (to, next) => {
    const loggedIn = await store.dispatch('session/login').then(async (response) => {
      if (!response || response.status !== 200) {
        if (to.path !== '/login') {
          await next('/login')
        }
        return false
      }
      return true
    })
    return loggedIn
  }
  Router.beforeResolve(async (to, from, next) => {
    let loggedIn = store.getters['session/loggedIn']
    if (!loggedIn) loggedIn = await tryToLogWithToken(to, next)
    return next()
  })
  return Router
}
