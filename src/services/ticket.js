import Vue from 'vue'

import store from 'src/store'
import { alert, getSocioPin } from 'src/helpers'

const itemsParaSubir = () => {
  return store.state.buffet.ticket.items.map((a) => {
    return {
      id: a.id,
      nombre: a.nombre,
      subNombre: a.subNombre,
      subId: a.subId,
      precio_venta: a.precio_venta,
      cantidad: a.cantidad
    }
  })
}
const assignSocio = (item, tipoItem, filter) => {
  store.dispatch('buffet/ticket/asignaSocio', item)
  // tipoItem.value = ''
  // filter.value = ''
}
const close = ({ cancela = true } = {}) => {
  store.dispatch('buffet/ticket/cierra', { cancela })
}
const cierra = async () => {
  store.dispatch('addLoading', 'cierra-ticket', { root: true })
  const total = store.getters['buffet/ticket/total']
  return Vue.prototype.$axios.post('/cierra', {
    doc: {
      socio: store.state.session.user,
      total,
      descuento: store.getters['buffet/ticket/descuento']
    },
    pendiente: true,
    items: itemsParaSubir()
  }).then(async (resp) => {
    const socioId = getSocioPin()
    const pin = {
      pin: resp.pin,
      id: resp.id,
      total,
      fecha: resp.fecha
    }
    await store.dispatch('buffet/setPins', pin, { root: true })
    await store.dispatch('buffet/ticket/cierra')
    await alert('Su compra fué guardada',
                          `<p>Para retirar su compra:</p>
                            <p>En la terminal de autogestión introduzca primero su pin personal
                            (<span class="pin">${socioId}</span>) seguido del PIN correspondiente
                            a esta compra (<span class="pin">${pin.pin}</span>), el código completo es ${socioId}${pin.pin}.</p>
                            <p>Estos códigos pueden ser consultados en la barra lateral</p>
                            <style>.pin{color:red;font-weight:400;font-size:110%}</style>`,
                          { html: true, timeout: 60000 })
    return 'ok'
  }).catch(function (error) {
    console.log('router push /', error)
    return error
  }).finally(r => {
    store.dispatch('removeLoading', 'cierra-ticket', { root: true })
  })
}
const addItem = (item) => {
  store.dispatch('buffet/ticket/itemAdd', item)
}
const getCarritoItemByIndex = (index) => {
  console.log('index', index, store.state.buffet.ticket.items)
  return store.state.buffet.ticket.items.find((_, _index) => _index === index)
}
const delItem = (item) => {
  store.dispatch('buffet/ticket/itemDelete', item)
}
const itemAddCant = (item, cantidad) => {
  store.dispatch('buffet/ticket/itemAddCant', { item, cantidad })
}
const itemModPrecio = (item, precio) => {
  store.dispatch('buffet/ticket/itemModPrecio', { item, precio })
}
const limpiaCarrito = (a) => {
  console.log('detalle', a)
  store.dispatch('buffet/ticket/limpia')
}
const getPin = (id) => {
  return Vue.prototype.$axios.get('/get-pin&id=' + id)
}
const rmPin = async (id) => {
  await Vue.prototype.$axios.get('/rm-pin&id=' + id)
  await store.dispatch('buffet/rmPin', id)
}
export { assignSocio, close, addItem, delItem, itemAddCant, itemModPrecio, getCarritoItemByIndex, limpiaCarrito, cierra, getPin, rmPin }
