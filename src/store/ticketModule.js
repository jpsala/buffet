import Vue from 'vue'
import store from '.'
const moduleState = {
  items: []
}
const moduleGetters = {
  neto (state, getters) {
    let total = 0
    state.items.forEach(item => {
      total += Number(item.total)
    })
    return total.toFixed(2)
  },
  descuento (state, getters) {
    let total = 0
    state.items.forEach(item => {
      if (item.tiene_descuento === '1') total += Number(item.total)
    })
    const _descuento = (total * getters.descuentoFactor)
    return _descuento.toFixed(2)
  },
  total (state, getters) {
    const total = getters.neto
    const descuento = getters.descuento
    return Number(total - descuento).toFixed(2)
  },
  clienteAsignado (state) {
    return Boolean(state.cliente && state.cliente.id)
  },
  socioNombre (state) {
    return state.cliente && state.cliente.nombre_completo
  },
  socioId (state) {
    return state.cliente && state.cliente.id
  },
  hayItems (state) {
    return state.items.length > 0
  },
  // esEventual (state) {
  //   return state.cliente && state.cliente.esEventual
  // },
  descuentoFactor (state, getters, rootState) {
    console.log('state.cliente && state.cliente.tipo', state.cliente)
    const esAlumno = !state.cliente || state.cliente.tipo === 'A'
    return esAlumno ? 0 : rootState.buffet.descuentoFactor
  }
}
const getItem = (item) => {
  return store.state.buffet.ticket.items.find(i => i.id === item.id)
}
const getItemIndex = (item) => {
  return store.state.buffet.ticket.items.findIndex(i => i.id === item.id)
}
const moduleActions = {
  itemDelete (ctx, item) {
    item = getItem(item)
    console.log('item', item)
    store.dispatch('buffet/cantidadCeroToItem', item, { root: true })
    ctx.commit('ARTICULO_DELETE', item)
  },
  itemAdd (ctx, item) {
    const itemEnTicket = getItem(item)

    store.dispatch('buffet/addCantidadToItem', { item, cantidad: 1 }, { root: true })
    if (itemEnTicket) {
      const cantidad = itemEnTicket.cantidad + 1
      ctx.commit('ITEM_CANTIDAD_SET', { item: itemEnTicket, cantidad })
    } else {
      const newItem = Object.assign({}, item)
      Vue.set(newItem, 'cantidad', 1)
      Vue.set(newItem, 'total', item.precio_venta)
      ctx.commit('TICKET_ARTICULO_ADD', newItem)
    }
  },
  itemAddCant (ctx, { item, cantidad }) {
    item = getItem(item)
    const nuevaCantidad = item.cantidad + cantidad
    if (nuevaCantidad < 1) {
      ctx.dispatch('itemDelete', item)
      return
    }
    store.dispatch('buffet/addCantidadToItem', { item, cantidad }, { root: true })
    ctx.commit('ITEM_CANTIDAD_SET', { item, cantidad: nuevaCantidad })
  },
  cierra (ctx) {
    ctx.dispatch('buffet/limpia', null, { root: true })
    ctx.commit('LIMPIA')
  },
  asignaSocio (ctx, cliente) {
    // Vue.set(cliente, 'esEventual', false)
    ctx.commit('ASIGNA_SOCIO', cliente)
  },
  logout (ctx) {
    // Vue.set(cliente, 'esEventual', false)
    ctx.commit('LOGOUT')
  },
  limpia (ctx) {
    ctx.dispatch('buffet/limpia', null, { root: true })
    ctx.commit('LIMPIA')
  }
}

const moduleMutations = {
  ARTICULO_DELETE (state, item) {
    state.items.splice(getItemIndex(item), 1)
  },
  TICKET_ARTICULO_ADD (state, item) {
    state.items.push(item)
  },
  ITEM_CANTIDAD_SET (state, { item, cantidad }) {
    item = getItem(item)
    Vue.set(item, 'cantidad', cantidad)
    Vue.set(item, 'total', item.cantidad * item.precio_venta)
  },
  ASIGNA_SOCIO (state, cliente) {
    state.cliente = cliente
    // state.descuento = state.cliente.tipo === 'E' ? 0.5 : 0
  },
  LOGOUT (state) {
    state.items = []
  },
  LIMPIA (state) {
    state.items = []
  }
}
export default {
  namespaced: true,
  getters: moduleGetters,
  mutations: moduleMutations,
  actions: moduleActions,
  state: moduleState
}
