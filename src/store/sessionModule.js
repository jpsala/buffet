import Vue from 'vue'
const moduleState = {
  apiToken: window.localStorage.getItem('buffet.quasar'),
  user: undefined,
  // eventual: undefined,
  descuentoEmpleado: 0
}

const moduleGetters = {
  loggedIn (state) {
    return Boolean(state.user)
  }
}

const moduleActions = {

  async login (context, user) {
    context.dispatch('addLoading', { id: 'login' }, { root: true })
    const ret = await Vue.prototype.$axios.post('login', user)
      .then(async (response) => {
        context.dispatch('removeLoading', 'login', { root: true })
        if (!response) throw Error('Error de conexión')
        if (response.status !== 200) return response
        // const { userData, eventual, descuento } = response
        const { userData, descuento, pins } = response
        context.dispatch('buffet/setPins', pins, { root: true })
        await context.dispatch('setLoginData', userData)
        // await context.dispatch('setSocioEventual', eventual)
        await context.dispatch('buffet/setDescuentoEmpleado', descuento, { root: true })
        return response
      })
      .catch((error) => {
        throw error
      })
    return ret
  },
  async logout ({ dispatch }) {
    await dispatch('buffet/setPins', undefined, { root: true })
    await dispatch('buffet/logout', null, { root: true })
    await dispatch('buffet/ticket/logout', null, { root: true })
    await dispatch('setApiToken', undefined)
    await dispatch('setLoginData', undefined)
    this.$router.push('/login')
  },
  setApiToken ({ commit }, value) {
    commit('API_TOKEN', value)
  },
  setLoginData ({ commit, dispatch }, data) {
    if (data) data.pin = data.id && data.id.padStart('0', 4).slice(-4)
    commit('USER', data)
  },
  setPins ({ commit }, pins) {
    commit('PINS', pins)
  },
  updateApiTokenFromInterceptor ({ dispatch }, value) {
    dispatch('setApiToken', value)
  }
}

const moduleMutations = {
  USER (state, data) {
    Vue.set(state, 'user', data)
  },
  API_TOKEN (state, value) {
    window.localStorage.setItem('buffet.quasar', value)
    state.apiToken = value
  }

}
export default {
  namespaced: true,
  getters: moduleGetters,
  mutations: moduleMutations,
  actions: moduleActions,
  state: moduleState
}
