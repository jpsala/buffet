
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', component: () => import('pages/Venta.vue') },
      { path: '/venta', component: () => import('pages/Venta.vue') },
      { path: '/ticket', component: () => import('pages/Ticket.vue') },
      { path: '/login', component: () => import('pages/Login.vue') },
      { path: '/logout', component: () => import('pages/Logout.vue') },
      { path: '/pin/:id?', component: () => import('pages/Pin.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
