import Vue from 'vue'
import store from 'src/store'
import colors from 'src/colors'

const getProductos = async (ctx) => {
  const resp = await Vue.prototype.$axios.get('articulos')
  const coloresUsados = []
  let random
  const getRandomColor = () => Math.floor(Math.random() * colors.length - 1 + 0)
  const categorias = resp.data.categorias.map(c => {
    random = getRandomColor()
    while (coloresUsados.includes(random)) {
      random = getRandomColor()
    }
    c.color = colors[random]
    coloresUsados.push(random)
    return c
  })
  const items = resp.data.articulos.filter(a => a.disponible === '1')
    .map(a => { a.cantidad = 0; return a })
  categorias.forEach(c => {
    c.items = items.filter(p => p.categoria_id === c.id)
  })
  store.commit('buffet/SET_CATEGORIAS', categorias)
  store.commit('buffet/SET_ITEMS', items)
}
export { getProductos }
