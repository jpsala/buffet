import { Dialog } from 'quasar'
import store from 'src/store'
const soloEnDevMode = func => {
  if (process.env.NODE_ENV === 'development') func()
}
const log = (...args) => {
  if (!process.env.NODE_ENV === 'development') return
  console.warn(...args)
  console.trace()
}
const alert = async (title, message, { html = false, timeout = false } = {}) => {
  let timeoutHandle
  let timeLeft
  let smallIntervaltHandle
  let btnEl
  return new Promise((resolve, reject) => {
    Dialog.create({
      title,
      message,
      html
    }).onOk(() => {
      resolve()
    }).onCancel(() => {
      reject()
    }).onDismiss(() => {
      console.log('dismiss')
      if (smallIntervaltHandle) {
        clearInterval(smallIntervaltHandle)
      }
      if (timeoutHandle) {
        clearTimeout(timeoutHandle)
      }
    })
    setTimeout(() => {
      if (timeout) {
        btnEl = document.querySelector('.q-dialog-plugin .q-btn__content div')
        console.log('btnEl', btnEl)
        timeLeft = timeout / 1000
        btnEl.innerText = `OK (${timeLeft})`
        timeoutHandle = setTimeout(() => {
          console.log('click')
          document.querySelector('.q-dialog-plugin.q-card button').click()
        }, timeout)
        smallIntervaltHandle = setInterval(() => {
          timeLeft -= 1
          btnEl.innerText = `OK (${timeLeft})`
        }, 1000)
      }
    }, 500)
  })
}
const confirm = async (title, msg) => {
  return new Promise((resolve) => {
    Dialog.create({
      title: title,
      message: msg,
      persistent: true,
      cancel: {
        label: 'Cancela'
      }
    }).onOk(() => {
      resolve(true)
    }).onCancel(() => {
      resolve(false)
    })
  })
}
const getSocioPin = () => {
  return store.state.session.user.id.padStart('0', 4).slice(-4)
}

export { soloEnDevMode, log, alert, confirm, getSocioPin }
