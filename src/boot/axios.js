import axios from 'axios'
// const locals = ['localhost']
// const hostname = document.location.hostname
// const local = locals.includes(hostname)
// const isServer = hostname === '192.168.2.254'
// const pcJP = window.localStorage.getItem('pcJP')
export default async ({ Vue, store }) => {
  // axios.defaults.baseURL = `http://${hostnmae}/iae/index.php?r=apiBuffet/`
  axios.defaults.baseURL = (process.env.NODE_ENV === 'production')
    ? 'http://iae.dyndns.org/iae/index.php?r=apiBuffetAlumnos/'
    : 'http://localhost/iae/index.php?r=apiBuffetAlumnos/'
  // axios.defaults.baseURL = `/iae/index.php?r=apiBuffet/`
  axios.interceptors.response.use((response) => {
    const endPoint = response.config.url.substring(response.config.url.lastIndexOf('/') + 1)
    console.log('endpoint %O %O response.data %O', endPoint, response, response.status === 500 ? `error 500: ${response.statusText}` : response.data)
    if (response.status === 500) throw response
    if (response.data && response.data.access_token && response.data.access_token !== 'undefined') {
      store.dispatch('session/updateApiTokenFromInterceptor', response.data.access_token)
    }
    return response.data
  }, (error) => {
    Promise.reject(error)
  })

  axios.interceptors.request.use(
    async (config) => {
      config.headers.Authorization = store.state.session.apiToken
      return config
    },
    error => Promise.reject(error)
  )

  axios.defaults.method = 'POST'
  axios.defaults.validateStatus = function (status) {
    return status >= 200 && status <= 503
  }
  Vue.prototype.$axios = axios
}
