import ticket from './ticketModule'

const moduleState = {
  items: [],
  categorias: [],
  descuentoFactor: 0,
  pins: undefined
}
const moduleGetters = {
}

const moduleActions = {
  setDescuentoEmpleado ({ commit }, data) {
    commit('DESCUENTO_EMPLEADO', data)
  },
  addCantidadToItem (ctx, { item, cantidad }) {
    // item = ctx.state.items.find(i => i.id === item.id)
    ctx.commit('ADD_CANTIDAD_TO_ITEM', { item, cantidad })
  },
  cantidadCeroToItem (ctx, item) {
    // item = ctx.state.items.find(i => i.id === item.id)
    ctx.commit('CANTIDAD_CERO_TO_ITEM', item)
  },
  limpia (ctx) {
    ctx.commit('LIMPIA')
  },
  logout (ctx) {
    ctx.commit('LOGOUT')
  },
  setPins (ctx, pins) {
    ctx.commit('PINS', pins)
  },
  rmPin (ctx, id) {
    const pinIdx = ctx.state.pins.findIndex(p => p.id === id)
    ctx.commit('RM_PIN', pinIdx)
  }
}

const moduleMutations = {
  SET_ITEMS (state, items) {
    state.items = items
  },
  SET_CATEGORIAS (state, categorias) {
    state.categorias = categorias
  },
  DESCUENTO_EMPLEADO (state, data) {
    state.descuentoFactor = data
  },
  ADD_CANTIDAD_TO_ITEM (state, { item, cantidad }) {
    const idx = state.items.findIndex(i => i.id === item.id)
    state.items[idx].cantidad = Number(state.items[idx].cantidad) + Number(cantidad)
  },
  CANTIDAD_CERO_TO_ITEM (state, item) {
    const idx = state.items.findIndex(i => i.id === item.id)
    state.items[idx].cantidad = 0
  },
  LIMPIA (state) {
    state.items.filter(i => i.cantidad).forEach(i => { i.cantidad = 0 })
  },
  LOGOUT (state) {
    state.items = []
    state.categorias = []
    state.pins = undefined
    state.descuentoFactor = 0
  },
  PINS (state, pins) {
    if (!pins) {
      state.pins = []
    // eslint-disable-next-line valid-typeof
    } else if (Object.prototype.toString.call(pins) === '[object Array]') {
      state.pins = pins
    } else {
      state.pins.push(pins)
    }
  },
  RM_PIN (state, idx) {
    state.pins.splice(idx, 1)
  }
}
export default {
  namespaced: true,
  getters: moduleGetters,
  mutations: moduleMutations,
  actions: moduleActions,
  state: moduleState,
  modules: {
    ticket
  }
}
